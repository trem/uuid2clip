# uuid2clip

Simple utility to copy a randomized UUID (v4) to the system clipboard.

Many Linux distributions have `uuidgen` pre-installed, which is perfectly fine, if you only need a UUID occasionally or on the terminal.
This utility is useful, if you frequently need UUIDs in GUI applications, e.g. as a software developer or data scientist.

