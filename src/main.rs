fn main() {
	let uuid = uuid::Uuid::new_v4();
	let uuid = uuid.to_string();

	cli_clipboard::set_contents(uuid.clone())
		.expect("Failed to set clipboard contents.");

	assert_eq!(cli_clipboard::get_contents().unwrap(), uuid);
}
